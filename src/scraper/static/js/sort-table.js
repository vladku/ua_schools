
window.onload = function() {
    var thead = document.getElementsByTagName("thead")[0];
    console.log(thead);    
    var cells = thead.getElementsByTagName("th"); 
    console.log(cells);    
    for (var i = 0; i < cells.length; i++) { 
        cells[i].onclick = sortTable;
        cells[i].setAttribute("index", i);
    }
};

function quicksort(n, arr, low, high) {
    if(low >= high) return;
    let pivotPosition = partition(n, arr, low, high);
    quicksort(arr,low, pivotPosition-1);
    quicksort(arr, pivotPosition+1, high);
}

function partition(n, arr, low, high) {
    let pivot = arr[high].getElementsByTagName("TD")[n].innerHTML.toLowerCase();
    let left = low, right = high-1;
    while(left < right) {
       while(arr[left].getElementsByTagName("TD")[n].innerHTML.toLowerCase()<pivot) {
            left++;
       }
       while(arr[right].getElementsByTagName("TD")[n].innerHTML.toLowerCase()>pivot) {
            right--; 
       }
       if(left >= right) {
            break;
       }
       let temp = arr[left];
       arr[left] = arr[right];
       arr[right] = temp;
    }
    let temp = arr[left];
    arr[left] = arr[high];
    arr[high] = temp;
    return left;
}

function sortTable() {
    n = this.getAttribute("index");
    table = this.parentNode.parentNode.parentNode;
    //var rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    switching = true;
    dir = "asc";
    var rows = table.rows;
    let len = rows.length;
    console.log(len);

    quicksort(n, rows, 1, len / 2);

    // for(let i = 1; i < len; i++) {
    //     let j = i;
    //     while(j > 1 && rows[j].getElementsByTagName("TD")[n].innerHTML.toLowerCase() 
    //         < rows[j-1].getElementsByTagName("TD")[n].innerHTML.toLowerCase()) {
    //         rows[j-1].parentNode.insertBefore(rows[j], rows[j-1]);
    //         j--;
    //     }
    // }


    // for (let i = 1; i < len; i++) {
    //     for (let j = 1; j < len; j++) {
    //         if (rows[j].getElementsByTagName("TD")[n].innerHTML.toLowerCase() 
    //             < rows[j + 1].getElementsByTagName("TD")[n].innerHTML.toLowerCase()) {             
    //             rows[j].parentNode.insertBefore(rows[j + 1], rows[j]);
    //         }
    //     }
    // }
    // while (switching) {
    //   switching = false;
    //   rows = table.rows;
    //   for (i = 1; i < (rows.length - 1); i++) {
    //     x = rows[i].getElementsByTagName("TD")[n];
    //     y = rows[i + 1].getElementsByTagName("TD")[n];
    //     console.log(x);        
    //     console.log(y);        
    //     if (dir == "asc") {
    //       if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
    //         // If so, mark as a switch and break the loop:
    //         shouldSwitch = true;
    //         break;
    //       }
    //     } else if (dir == "desc") {
    //       if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
    //         // If so, mark as a switch and break the loop:
    //         shouldSwitch = true;
    //         break;
    //       }
    //     }
    //   }
    //   if (shouldSwitch) {
    //     /* If a switch has been marked, make the switch
    //     and mark that a switch has been done: */
    //     rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
    //     switching = true;
    //     // Each time a switch is done, increase this count by 1:
    //     switchcount ++;
    //   } else {
    //     /* If no switching has been done AND the direction is "asc",
    //     set the direction to "desc" and run the while loop again. */
    //     if (switchcount == 0 && dir == "asc") {
    //       dir = "desc";
    //       switching = true;
    //     }
    //   }
    // }
  }