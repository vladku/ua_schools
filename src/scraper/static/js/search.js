function myFunction() {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementsByTagName("table")[0];
    tr = table.getElementsByTagName("tr");
  
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        tds = tr[i].getElementsByTagName("td");
      //if (td) {
        for (j = 0; j < tds.length; j++) {
            var td = tds[j]
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
                break;
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function updateAria(btn, name) {
    btn.classList.toggle("hiden-spinner");
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/schools/update/" + name, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                console.log(xhr.responseText);
            } else {
                console.error(xhr.statusText);
            }
            btn.classList.toggle("hiden-spinner");
        }
    };
    xhr.onerror = function (e) {
        btn.classList.toggle("hiden-spinner");
        console.error(xhr.statusText);
    };
    xhr.send(null);
}
