# compose_flask/app.py
from flask import Flask, render_template, jsonify
from flask_sqlalchemy import SQLAlchemy
from redis import StrictRedis
import pandas as pd
import scraper

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config ['SQLALCHEMY_DATABASE_URI'] = 'postgresql://user:pass@db:5432/schools_db'

db = SQLAlchemy(app)

class School(db.Model):
    __tablename__ = 'schools'

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String())
    name = db.Column(db.String())
    fullname = db.Column(db.String())
    type = db.Column(db.String())
    post_index = db.Column(db.String())
    post_address = db.Column(db.String())
    phone_number = db.Column(db.String())
    director = db.Column(db.String())
    agent = db.Column(db.String())
    language = db.Column(db.String())
    number_of_student = db.Column(db.String())
    number_of_staff = db.Column(db.String())
    number_of_classes = db.Column(db.String())
    number_of_rooms = db.Column(db.String())
    email = db.Column(db.String())
    aria_id = db.Column(db.Integer, db.ForeignKey('arias.id'),  nullable=False)
    aria = db.relationship('Aria', lazy=False)

    def __init__(self, url, email, aria):
        self.url = url
        self.email = email
        self.aria_id = aria.id
        self.aria = aria

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def __str__(self):
        return f"{self.name=}, {self.email=}, {self.aria_id=}"

    def to_dict(self):
        return {scraper.fields[k] if k in scraper.fields else k: v for k, v in self.__dict__.items() if not k.startswith('_') and not "id" in k}
        
class Aria(db.Model):
    __tablename__ = 'arias'

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(), unique=True)
    name = db.Column(db.String())

    def __init__(self, name, url):
        self.url = url
        self.name = name

    def __repr__(self):
        return f'{self.name}'

    def to_dict(self):
        return {k: v for k, v in self.__dict__.items() if not k.startswith('_') and not "id" in k}
    
db.create_all()

@app.route('/')
def hello():
    return jsonify(['%s' % redis.get(x) for x in redis.keys("services*")])

def get_all_dicts(table):
    all = table.query.all()
    return pd.DataFrame([s.to_dict() for s in all])

def get_template(df, name):
    return render_template(f'{name}.html', 
      tables=[df.to_html(classes='data', header="true")],
      menu=[menu_item.format(*x.values()) for x in menu_items])

@app.route('/schools')
def schools():
    return get_template(get_all_dicts(School), 'schools')

@app.route('/arias/<int:x>/schools')
def schoolsi(x):
    df = pd.DataFrame([s.to_dict() for s in School.query.filter(School.aria_id==x)])
    return get_template(df, 'schools')

@app.route('/arias/<string:x>/schools')
def schoolss(x):
    a = Aria.query.filter(Aria.name == x).first()
    df = pd.DataFrame([s.to_dict() for s in School.query.filter(School.aria_id==a.id)])
    return get_template(df, 'schools')

def get_aria(name):
    return Aria.query.filter(Aria.name == name).first()


@app.route('/aria/<string:aria_name>')
def eeeschools_update(aria_name):
    aria = get_aria(aria_name)
    return str(aria)

@app.route('/schools/update/<string:aria_name>')
def schools_update(aria_name):
    aria = get_aria(aria_name)
    url = scraper.get_schools_list_url(aria)
    scraper.add_schools(School, db, url, aria)
    db.session.commit()
    db.session.close()
    return "True"

@app.route('/arias/update')
def arias_update():
    arias_current = {x.name: x for x in Aria.query.all()}
    for name, url in scraper.get_arias():
        url = f"https:{url}"
        if name in arias_current:
            arias_current[name].url = url
            continue
        aria = Aria(name, url)
        db.session.add(aria)
    db.session.commit()
    db.session.close()
    return "True"

menu_items = [
    {
        "url": "/schools",
        "icon": "fa fa-home",
        "text": "Home"
    },
    {
        "url": "/schools",
        "icon": "fas fa-school",
        "text": "Schools"
    },
    {
        "url": "/arias",
        "icon": "fas fa-globe-europe",
        "text": "Arias"
    },
    {
        "url": "#",
        "icon": "fas fa-envelope-open-text",
        "text": "eMails"
    }
]
menu_item = """
<li>                                 
    <a href="{}">
        <i class="{}"></i>
        <span class="nav-text">{}</span>
    </a>
</li> 
"""

@app.route('/arias')
def arias():    
    all = [x.to_dict() for x in Aria.query.all()]
    return render_template('arias.html', 
      arias=all,
      menu=[menu_item.format(*x.values()) for x in menu_items])


if __name__ == "__main__":
    redis = StrictRedis(host='redis', port=6379, charset="utf-8", decode_responses=True)
    app.run(host="0.0.0.0", debug=True)
