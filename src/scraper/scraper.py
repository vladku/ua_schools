import aiohttp
import asyncio
import requests
import re

from lxml import html
import urllib.parse as up

fields = {
    "fullname": "Повна назва:",
    "name": "Скорочена:",
    "type": "Тип ЗЗСО:",
    "post_index": "Індекс:",
    "post_address": "Поштова адреса:",
    "phone_number": "Телефони:",
    "director": "Директор:",
    "agent": "Уповноважена особа:",
    "language": "Мова навчання:",
    "number_of_student": "Кількість учнів:",
    "number_of_staff": "Кількість персоналу:",
    "number_of_classes": "Кількість класів:",
    "number_of_rooms": "Кількість приміщень:"
}

async def get_email(url):
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                tree = html.fromstring(await resp.text())
                email = tree.xpath('//tr[th/text()="E-mail:"]/td/a')
                if not email:
                    return url, None, tree
                email = email[0].attrib["onclick"]
                email = [x.groupdict() for x in re.finditer(r"unescape\('(?P<t>[^']*)", email)][0]["t"]
                email = html.fromstring(up.unquote(email)).xpath("//a/text()")[0]
                return url, email, tree
    except:
        return url, None, None

def get_field(tree, name):
    r = tree.xpath(f"//tr[th/text()='{name}']/td/text()")
    return r[0] if r else None

def get_arias():    
    page = html.fromstring(requests.get("https://isuo.org/").content)
    arias = page.xpath("//ul[@id='side-menu']/li[not(@id='mon-link')]/a")
    return [(x.text, x.attrib['href'][:-1]) for x in arias]

def get_schools_list_url(aria):
    page = html.fromstring(requests.get(aria.url).content)
    aria_info = page.xpath("//div[@id='admin-struct']/ul/li/ul/li/a")[0]
    return f"{aria.url}{aria_info.attrib['href'].replace('view', 'schools-list')}"

def get_schools_urls(aria, url, page_number):    
    page = requests.get(f"{url}/page/{page_number}").content
    tree = html.fromstring(page)
    count = tree.xpath("//div[@class='list-type-select']/div/text()")[0]
    count = int(re.search(r"\d+", count)[0])
    refs = [f"{aria.url}{x.attrib['href']}" for x in tree.xpath("//td/a")]
    return count, refs

def add_schools(School, db, url, aria):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    i, p, count = 0, 1, 1
    while i < count:
        count, refs = get_schools_urls(aria, url, p)
        i += len(refs)
        tasks = []
        for x in refs:
            task = asyncio.ensure_future(get_email(x))
            tasks.append(task)
        loop.run_until_complete(asyncio.wait(tasks))
        responses = loop.run_until_complete(asyncio.gather(*tasks))
        for r, e, tree_fields in responses:
            school = School.query.filter(School.email == e).first()
            if not school:
                school = School(r, e, aria)
            for f_n, field in fields.items():
                if tree_fields is not None:
                    setattr(school, f_n, get_field(tree_fields, field))
            db.session.merge(school)
        p += 1





