package main

import (
    "fmt"
    "log"
    "net/http"
    "encoding/json"
	"github.com/gorilla/mux"
	"database/sql"
	"context"
	"github.com/go-redis/redis"
	
	_ "github.com/lib/pq"
)

const (
	host     = "db"
	port     = 5432
	user     = "user"
	password = "pass"
	dbname   = "schools_db"
  )

type School struct {
	Id int `json:"id"`
    Name string `json:"name"`
    Email sql.NullString `json:"email"`
    Url string `json:"url"`
    Aria string `json:"aria"`
}

var Schools []School

func homePage(w http.ResponseWriter, r *http.Request){
    fmt.Fprintf(w, "Welcome to the HomePage!")
}

func returnAllSchools(w http.ResponseWriter, r *http.Request){
    fmt.Println("Endpoint Hit: returnAllSchools")
    json.NewEncoder(w).Encode(Schools)
}

func handleRequests() {
    // creates a new instance of a mux router
    myRouter := mux.NewRouter().StrictSlash(true)
    // replace http.HandleFunc with myRouter.HandleFunc
    myRouter.HandleFunc("/", homePage)
    myRouter.HandleFunc("/schools/all", returnAllSchools)
    // finally, instead of passing in nil, we want
    // to pass in our newly created router as the second
    // argument
    log.Fatal(http.ListenAndServe(":10000", myRouter))
}

var ctx = context.Background()

func main() {
	fmt.Println("Rest API v2.0 - Mux Routers")

    rdb := redis.NewClient(&redis.Options{
        Addr:     "redis:6379",
        //Password: "", // no password set
        DB:       0,  // use default DB
    })

	
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
    "password=%s dbname=%s sslmode=disable",
	host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	//rows, err := db.Query("SELECT * FROM schools LIMIT $1", 3)
	rows, err := db.Query(`SELECT schools.Id, schools.Url, schools.Name, Email, arias.Name Aria
						FROM schools AS schools
						INNER JOIN arias AS arias ON schools.aria_id=arias.id`)
	if err != nil {
		log.Fatal("Failed to execute query: ", err)
	}
	defer rows.Close()
	for rows.Next() {
		var school School
		err = rows.Scan(&school.Id, &school.Url, &school.Name, &school.Email, &school.Aria)
		if err != nil {
			panic(err)
		}
		Schools = append(Schools, school)
		//fmt.Printf("Hi %s, welcome back!\n", school.Email)
		//fmt.Println(id, firstName)
	}


	val, err := rdb.Do(ctx, "keys", "services*").Result()
	if err != nil {
		if err == redis.Nil {
			fmt.Println("key does not exists")
			return
		}
		panic(err)
	}
	fmt.Println(val)
	rdb.Set(ctx, "services:2", "go", 0)

	handleRequests()

	defer fmt.Println("!")
}
